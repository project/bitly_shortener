# Bitly Shortener

## Table of contents

- Introduction
- Requirements
- Installation
- Configuration
- Maintainers


## Introduction

This Bitly Shortener module integrates drupal sites with the Bitly URL shortening service.
We can use it in a custom module and twig template.

- For a full description of the module, visit the
  [project page](https://www.drupal.org/project/bitly_shortener).
- Submit bug reports and feature suggestions, or track changes in the
  [issue queue](https://www.drupal.org/project/issues/bitly_shortener).


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Visit the configuration page at Home >> Administration >> Configuration
   Bitly Shortener API page.
1. Enable module feature by checked Bitly Shortener Enable.
1. Create an account on Bitly. Save the access token in the Bitly shortener configuration form.
   [Configuration Form](https://app.bitly.com/settings/api/).


## Maintainers

Supporting by:

- Deepak Bhati (heni_deepak) - https://www.drupal.org/u/heni_deepak
- Radheshyam Kumawat (radheymkumar) - https://www.drupal.org/u/radheymkumar
