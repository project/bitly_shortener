<?php

namespace Drupal\bitly_shortener\Services;

/**
 * BitlyShortenerServicesInterface.
 */
interface BitlyShortenerServicesInterface {

  /**
   * Get a shortener.
   *
   *   The options for the shortener.
   */
  public function shortener($url);

}
