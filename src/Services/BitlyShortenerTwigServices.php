<?php

namespace Drupal\bitly_shortener\Services;

use Twig\Extension\AbstractExtension;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Theme\ThemeManagerInterface;
use Twig\TwigFunction;

/**
 * Extend Drupal's Twig_Extension class.
 */
class BitlyShortenerTwigServices extends AbstractExtension {

  /**
   * The module handler to invoke alter hooks.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The theme manager to invoke alter hooks.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  protected $themeManager;

  /**
   * Constructs the TwigTweakExtension object.
   */
  public function __construct(ModuleHandlerInterface $module_handler, ThemeManagerInterface $theme_manager) {
    $this->moduleHandler = $module_handler;
    $this->themeManager = $theme_manager;
  }

  /**
   * {@inheritdoc}
   * Return your twig function.
   */
  public function getFunctions(): array {

    $functions = [
      new TwigFunction('bitly_shortener', [self::class, 'bitlyShortener']),
    ];

    $this->moduleHandler->alter('bitly_shortener_functions', $functions);
    $this->themeManager->alter('bitly_shortener_functions', $functions);

    return $functions;
  }

  /**
   * Return bitly url.
   * {{ bitly_shortener('https://www.drupal.org') }}
   */
  public static function bitlyShortener($url) {
    $client = \Drupal::service('bitly_shortener')->shortener($url);
    return $client;
  }

}
