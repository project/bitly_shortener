<?php

namespace Drupal\bitly_shortener\Services;

use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * BitlyShortenerServices.
 */
class BitlyShortenerServices implements BitlyShortenerServicesInterface {

  /**
   * Protected configFactory variable.
   *
   * @var Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, MessengerInterface $messenger) {
    $this->configFactory = $config_factory;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('messenger')
    );
  }

  /**
   * Get shortener url.
   */
  public function shortener($url) {

    if (($this->configFactory->get('bitly_shortener.settings')->get('bitly_shortener_enable') == 0)
      && empty($this->configFactory->get('bitly_shortener.settings')->get('bitly_shortener_token'))
    ) {
      return $url;
    }

    $bitly_api = $this->configFactory->get('bitly_shortener.settings')->get('bitly_shortener_api');
    $bitly_token = $this->configFactory->get('bitly_shortener.settings')->get('bitly_shortener_token');

    try {
      $data = [
        'long_url' => $url,
      ];
      $payload = json_encode($data);
      $header = [
        'Authorization: Bearer ' . $bitly_token,
        'Content-Type: application/json',
        'Content-Length: ' . strlen($payload),
      ];

      $ch = curl_init($bitly_api);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
      curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
      $result = curl_exec($ch);
      $resultToJson = json_decode($result);

      if (isset($resultToJson->link)) {
        return $resultToJson->link;
      }
      else {
        $this->messenger->addWarning('Bitly shortener invalid access token.');
        return $url;
      }
    }
    catch (Exception $e) {
      return $e->getMessage();
    }
  }

}
