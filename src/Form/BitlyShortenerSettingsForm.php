<?php

namespace Drupal\bitly_shortener\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class BitlyShortenerSettingsForm.
 *
 *    @package Drupal\bitly_shortener\Form
 */
class BitlyShortenerSettingsForm extends ConfigFormBase {

  /**
   * Drupal\bitly_shortener\Services\BitlyShortenerServicesInterface definition.
   *
   * @var \Drupal\bitly_shortener\Services\BitlyShortenerServicesInterface
   */
  protected $bitlyShortener;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->bitlyShortener = $container->get('bitly_shortener');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bitly_shortener_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['bitly_shortener.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $settings = $this->config('bitly_shortener.settings');

    if ($settings->get('bitly_shortener_enable') != FALSE) {
      $bitly_url = $this->bitlyShortener->shortener('https://www.drupal.org/');
      $this->messenger()->addMessage('Bitly Shortener Status: ' . $bitly_url);
    }

    // General message time form settings.
    $form['bitly_shortener_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Bitly Shortener'),
      '#open' => TRUE,
    ];

    $form['bitly_shortener_settings']['bitly_shortener_enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Bitly Shortener Enable'),
      '#default_value' => $settings->get('bitly_shortener_enable'),
    ];

    $form['bitly_shortener_settings']['bitly_shortener_api'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Bitly Shortener Api'),
      '#default_value' => $settings->get('bitly_shortener_api'),
      '#states' => [
        'required' => [
          ':input[name="bitly_shortener_enable"]' => [
            'checked' => TRUE,
          ],
        ],
        'visible' => [
          ':input[name="bitly_shortener_enable"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    $form['bitly_shortener_settings']['bitly_shortener_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Access Token'),
      '#description' => $this->t('Generate access token. <a href="https://app.bitly.com/settings/api" target="_blank">https://app.bitly.com/settings/api/</a>'),
      '#default_value' => $settings->get('bitly_shortener_token'),
      '#states' => [
        'required' => [
          ':input[name="bitly_shortener_enable"]' => [
            'checked' => TRUE,
          ],
        ],
        'visible' => [
          ':input[name="bitly_shortener_enable"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if (!preg_match('/^[a-zA-Z0-9]{40}+$/', $values['bitly_shortener_token'])) {
      $form_state->setErrorByName('bitly_shortener_token', $this->t('Invalid bitly access token.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('bitly_shortener.settings')
      ->set('bitly_shortener_token', $form_state->getValue('bitly_shortener_token'))
      ->set('bitly_shortener_api', $form_state->getValue('bitly_shortener_api'))
      ->set('bitly_shortener_enable', $form_state->getValue('bitly_shortener_enable'))
      ->save();
  }

}
