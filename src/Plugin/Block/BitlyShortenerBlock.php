<?php

namespace Drupal\bitly_shortener\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\bitly_shortener\Services\BitlyShortenerServicesInterface;
use Drupal\Core\Url;

/**
 * Create a block for bitly_shortener block.
 *
 * @Block(
 *   id = "bitly_shortener_block",
 *   admin_label = @Translation("Bitly Shortener"),
 *   category = @Translation("Bitly Shortener")
 * )
 */
class BitlyShortenerBlock extends BlockBase implements ContainerFactoryPluginInterface {
  /**
   * BitlyShortenerServicesInterface.
   *
   * @var Drupal\bitly_shortener\Services\BitlyShortenerServicesInterface
   */
  protected $bitlyShortener;

  /**
   * Construct.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\bitly_shortener\Services\BitlyShortenerServicesInterface $bitlyShortener
   *   The BitlyShortenerServicesInterface service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, BitlyShortenerServicesInterface $bitlyShortener) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->bitlyShortener = $bitlyShortener;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('bitly_shortener')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $current_url = Url::fromRoute('<current>', [], ["absolute" => TRUE])->toString();
    $client = $this->bitlyShortener->shortener($current_url);

    return [
      '#theme' => 'bitly_shortener_block',
      '#bitly_shortener' => $client,
      '#attached' => [
        'library' => [
          'bitly_shortener/bitly_shortener',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
