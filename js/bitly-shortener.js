/**
 * @file
 * JS file for copyToclipboard.
 */

function bitlyShortenerFunction() {
    let copyText = document.getElementById("shortener-link");
    /* Select the text field */
    copyText.select();
    copyText.setSelectionRange(0, 99999); /* For mobile devices */
    navigator.clipboard.writeText(copyText.value);

    let tooltip = document.getElementById("shortener-tooltip-text");
    tooltip.innerHTML = "Copied: " + copyText.value;
}

function bitlyShortenerOutFunc() {
    let tooltip = document.getElementById("shortener-tooltip-text");
    tooltip.innerHTML = "Copy to clipboard";
}